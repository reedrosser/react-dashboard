var React = require('react');
var Tab = require('./Tab.jsx');

var SplitBlock = React.createClass({
  render: function() {

    var topRow = {
      height: 50,
      borderRadius: 5,
      color: 'white',
      paddingLeft: 25,
      background: 'blue'
    };

    var bottomRow = {
      height: 50,
      background: 'white'
    }
    var filler100 = {
      height: 75,
      background: 'white'
    };

    var boxLabel = {
      background: 'white',
      borderTopRightRadius: 5,
      borderTopLeftRadius: 5,
      height: 75,
      marginTop: 10,
      paddingLeft: 10,
      paddingTop: 5
    }
    var tabStyle = {};

    if (this.props.tabStyle) {
      tabStyle = this.props.tabStyle;
    }

    if (this.props.color) {
      boxLabel.background = this.props.color;
    }

    var createBlock = function(splitBlock, index) {
      return (
        <div className='row' key={index + splitBlock.top}>
          <div className='col-sm-12'>
            <div style={boxLabel}>
              <Tab tabStyle={tabStyle} top={splitBlock.top} bottom={splitBlock.bottom}/>
            </div>
          </div>
          <div className='col-sm-12'>
            <div style={filler100}>
            </div>
          </div>
        </div>
      );
    };

    return (
      <div className={this.props.splitBlock.width}>
        {createBlock(this.props.splitBlock)}
      </div>
    );
  }
});

module.exports = SplitBlock;
