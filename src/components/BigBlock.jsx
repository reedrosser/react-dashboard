var React = require('react');
var SkinnyBlock = require('./SkinnyBlock.jsx');

var BigBlock = React.createClass({
  render: function() {
    var bigFatty = {
      background: '#FFAAFF',
      height: 300,
      marginTop: 10
    };

    if (this.props.block.bigBlockStyle) {
      bigFatty = this.props.block.bigBlockStyle;
    }

    var createBlock = function(block, index) {
      return (
        <div className={block.bigBlockWidth} key={index + block.bigBlockWidth}>
          <div style={bigFatty}>
            <div className='row'>
              <SkinnyBlock block={block.skinnyBlock}/>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div className='row'>{createBlock(this.props.block)}</div>
    );
  }
});

module.exports = BigBlock;
