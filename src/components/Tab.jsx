var React = require('react');

var Tab = React.createClass({
  render: function() {

    var topStyle = {};
    var bottomStyle = {};
    var tabDivStyle = {};

    if (this.props.tabStyle) {
      topStyle = this.props.tabStyle.top;
      bottomStyle = this.props.tabStyle.bottom;
    }
    if (this.props.tabDivStyle) {
      tabDivStyle = this.props.tabDivStyle;
    }
    return (
      <div style={tabDivStyle}>
        <p style={topStyle}>{this.props.top}</p>
        <p style={bottomStyle}>{this.props.bottom}</p>
      </div>
    );
  }
});

module.exports = Tab;
