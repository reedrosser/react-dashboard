var React = require('react');
var Tab = require('./Tab.jsx');

var SkinnyBlock = React.createClass({

  render: function() {

    var tabBlockStyle = {

    }

    var innerSkinny = {
      background: 'red'
    };

    if(this.props.block.skinnyBlockStyle) {
      innerSkinny = this.props.block.skinnyBlockStyle;
    };



    var createTabBlock = function(block, index) {

      if (block.tabBlock) {
        tabBlockStyle = block.tabBlockStyle;
      }
      return (
        <div className={block.width} key={index + block.top}>
          <div style={tabBlockStyle}>
            <Tab tabStyle={block.tabStyle} top={block.top} bottom={block.bottom} tabDivStyle={block.tabDivStyle}/>
          </div>
        </div>
      );
    };

    var createSkinnyBlock = function(blocks, index) {
      return (
        <div className='row'>
          {blocks.map(createTabBlock)}
        </div>
      );
    };

    return (
      <div className={this.props.block.width}>
        <div className='theskinnyblock' style={innerSkinny}>
          {createSkinnyBlock(this.props.block.tabBlocks)}
        </div>
      </div>
    );
  }


});

module.exports = SkinnyBlock;
