var React = require('react');
var BasicBlock = require('./BasicBlock.jsx');
var SplitBlock = require('./SplitBlock.jsx');
var BigBlock = require('./BigBlock.jsx');

var DashboardManager = React.createClass({

  getInitialState: function() {
    return {
      blocks: [
        {width: 'col-sm-4', top: "20", bottom: "New followers this month"},
        {width: 'col-sm-4', top: "$1250", bottom: "Average Monthly Incomde"},
        {width: 'col-sm-4', top: "$13865", bottom: "Yearly Income Goal"},
        {width: 'col-sm-12', top: "18", bottom: "Paris"}
      ],
      splitBlocks: [
        {width: 'col-sm-12', top: "New Visitors", bottom: "1.5K"},
        {width: 'col-sm-12', top: "Bounce Rate", bottom: "50%"},
        {width: 'col-sm-12', top: "Searches", bottom: "28%"},
        {width: 'col-sm-12', top: "Traffic", bottom: "140.5kb"},
      ]
    };
  },

  render: function() {

    var innerBlock = {
      borderRadius: 5,
      height: 200,
      paddingTop: 75,
      paddingLeft: 20
    };
    var temperatureBlock = {
      borderRadius: 5,
      height: 200,
      paddingTop: 25,
      textAlign: 'center'
    };
    var standardLabel = {
      top: {
        color: '#656565',
        fontSize: 24,
        fontWeight: 'bold'
      },
      bottom: {
        color: '#656565'
      }
    }

    var skinnyBlockLabel = {
      top: {
        color: 'white',
        fontSize: 24
      },
      bottom: {
        color: 'white'
      }
    }

    var splitLabel = {
      top: {
        color: 'white'
      },
      bottom: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 28
      }
    }

    var temperatureLabel = {
      top: {
        color: 'white',
        fontSize: 48,
        fontWeight: 'bold'
      },
      bottom: {
        color: 'white'
      }
    }

    var bigBlock1 = {
      bigBlockStyle: {
        background: '#0096d0',
        borderRadius: 5,
        height: 300,
        marginTop: 10
      },
      bigBlockWidth: 'col-sm-12',
      skinnyBlock: {
        width: 'col-sm-12',
        skinnyBlockStyle: {
          background: '#484d4d',
          borderBottomLeftRadius: 5,
          borderBottomRightRadius: 5,
          height: 100,
          marginTop: 200
        },
        tabBlocks: [
          {
            width: 'col-sm-4',
            top: "15080",
            bottom: 'Shot Views',
            tabStyle: skinnyBlockLabel,
            tabBlockStyle: {height: 100},
            tabDivStyle: {
              paddingTop: 15,
              textAlign: 'center'
            },
            textAlign: 'center'
          },
          {
            width: 'col-sm-4',
            top: "12000",
            bottom: 'Likes',
            tabStyle: skinnyBlockLabel,
            tabDivStyle: {
              paddingTop: 15,
              textAlign: 'center'
            },
            tabBlockStyle: {height: 100}
          },
          {
            width: 'col-sm-4',
            top: "5100",
            bottom: 'Comments',
            tabStyle: skinnyBlockLabel,
            tabDivStyle: {
              paddingTop: 15,
              textAlign: 'center'
            },
            tabBlockStyle: {height: 100}
          }
        ]

      }
    };

    var bigBlock2 = {
      bigBlockStyle: {
        background: '#cd59ae',
        borderRadius: 5,
        height: 300,
        marginTop: 10
      },
      bigBlockWidth: 'col-sm-12',
      skinnyBlock: {
        width: 'col-sm-12',
        skinnyBlockStyle: {
          background: '#484d4d',
          borderBottomLeftRadius: 5,
          borderBottomRightRadius: 5,
          height: 100,
          marginTop: 200
        },
        tabBlocks: [
          {
            width: 'col-sm-4',
            top: "15080",
            bottom: 'Shot Views',
            tabStyle: skinnyBlockLabel,
            tabBlockStyle: {height: 100},
            tabDivStyle: {
              paddingTop: 15,
              textAlign: 'center'
            },
            textAlign: 'center'
          },
          {
            width: 'col-sm-4',
            top: "12000",
            bottom: 'Likes',
            tabStyle: skinnyBlockLabel,
            tabDivStyle: {
              paddingTop: 15,
              textAlign: 'center'
            },
            tabBlockStyle: {height: 100}
          },
          {
            width: 'col-sm-4',
            top: "5100",
            bottom: 'Comments',
            tabStyle: skinnyBlockLabel,
            tabDivStyle: {
              paddingTop: 15,
              textAlign: 'center'
            },
            tabBlockStyle: {height: 100}
          }
        ]

      }
    };

    return (
      // <BasicBlock top="Test" bottom="Sub Test"/>
      <span>
        <div className='row'>
          <h1>{this.props.title}</h1>
        </div>
        <div className='row'>
          <div className='col-sm-9'>
            <div className='row'>
              <BasicBlock blocks={this.state.blocks[0]} innerBlock={innerBlock} tabStyle={standardLabel} color='white'/>
              <BasicBlock blocks={this.state.blocks[1]} innerBlock={innerBlock} tabStyle={standardLabel} color='white'/>
              <BasicBlock blocks={this.state.blocks[2]} innerBlock={innerBlock} tabStyle={standardLabel} color='white'/>
            </div>
              <BigBlock block={bigBlock1} />
              <BigBlock block={bigBlock2} />
              {/* <BigBlock block={{left: 'here', right: 'there', middle: 'everywhere'}} innerBlock={innerBlock} tabStyle={standardLabel} color='white'/> */}

          </div>
          <div className='col-sm-3'>
            <div className='row'>
              <BasicBlock blocks={this.state.blocks[3]} innerBlock={temperatureBlock} tabStyle={temperatureLabel} color='#ff8a00'/>
              <SplitBlock splitBlock={this.state.splitBlocks[0]} tabStyle={splitLabel} color='#0096d0'/>
              <SplitBlock splitBlock={this.state.splitBlocks[1]} tabStyle={splitLabel} color='#b28ad6'/>
              <SplitBlock splitBlock={this.state.splitBlocks[2]} tabStyle={splitLabel} color='#ff4826'/>
              <SplitBlock splitBlock={this.state.splitBlocks[3]} tabStyle={splitLabel} color='#63c254'/>
            </div>
            {/* <SplitBlock splitBlock={this.state.splitBlocks} blockStyle={whiteBlock}/> */}
          </div>
        </div>
    </span>

    );
  }

});

module.exports = DashboardManager;
