var React = require('react');
var Tab = require('./Tab.jsx');

var BasicBlock = React.createClass({

  render: function() {

    var tempBlock = {top: 'testing', bottom: 'subtesting'};

    var blockStyle = {

    };

    var innerBlock = {};

    var tabStyle = {};

    if (this.props.innerBlock) {
      innerBlock = this.props.innerBlock;
      innerBlock.background = this.props.color;
    }

    if (this.props.tabStyle) {
      tabStyle = this.props.tabStyle;
    }

    var createBlock = function(block, index) {
      return (
        <div className={block.width} key={index + block.top}>
          <div className='innerBlock' style={innerBlock}>
            <Tab tabStyle={tabStyle} top={block.top} bottom={block.bottom}/>
          </div>
        </div>
      );
    };

    return (createBlock(this.props.blocks));
    // return (<div style={blockStyle} className="col-sm-3">{this.props.blocks.map(createTab)}</div>);
  }
})

module.exports = BasicBlock;
