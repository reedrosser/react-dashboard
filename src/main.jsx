var React = require('react');
var ReactDOM = require('react-dom');
var DashboardManager = require('./components/DashboardManager.jsx');

ReactDOM.render(<DashboardManager title="Dashboard" />, document.getElementById('dashboard'));
